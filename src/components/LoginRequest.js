import { Box, Divider } from "@material-ui/core";
import Typography from '@material-ui/core/Typography';

function LoginRequest(props) {
    return (
        <>
            <Box>
                <Typography variant="body2" color="textSecondary" align="center">
                    Запрос:
                </Typography>
                <Typography variant="body2" color="textSecondary" align="center">
                    Email: {props.Email}
                </Typography>
                <Typography variant="body2" color="textSecondary" align="center">
                    Password: {props.Password}
                </Typography>
                <Divider/>
            </Box>
        </>
    );
}

export default LoginRequest;