import React from 'react';
import { Link as MaterialLink } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

function NavigationBar() {

	const navStyle = {
		color: 'white',
		marginRight: '20px',
		textDecoration: 'none'
	}


	const classes = useStyles();
	return (

		<AppBar position="static" color="primary">
			<Toolbar>
				<IconButton edge="start" color="inherit" aria-label="menu" component={Link} to="/">
					<HomeIcon />
				</IconButton>
				<Typography variant="h6" className={classes.title} style={navStyle} component={Link} to="/">
					ReduxProject
				</Typography>
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/login">LOGIN</MaterialLink>
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/register">REGISTER</MaterialLink>
			</Toolbar>
		</AppBar>
	);
}

export default NavigationBar;
