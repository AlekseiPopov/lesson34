import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'center',
        paddingTop: theme.spacing(10)
    },
}));

function NavigationBar() {

    const classes = useStyles();
    return (
        <>
            <Typography variant="h1" className={classes.title}>
                Error 404
			</Typography>
            <Typography variant="h2" color="textSecondary" className={classes.title}>
                Sorry, page not found!
            </Typography>
        </>
    );
}

export default NavigationBar;
