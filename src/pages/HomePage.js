import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement, login, logout } from '../actions';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3),
    },
    title: {
        textAlign: 'center'
    }
}));

export default function HomePage() {
    const classes = useStyles();
    const counter = useSelector(state => state.counter);
    const isLogged = useSelector(state => state.isLogged);
    const dispatch = useDispatch();


    return (
        <>
            <Typography variant="h3" className={classes.title}>
                Counter through Redux: {counter}
            </Typography>

            <Button
                onClick={() => dispatch(increment(5))}
                variant="contained"
                className={classes.submit}
                color="primary">Прибавить 5</Button>
            <Button
                onClick={() => dispatch(decrement())}
                variant="contained"
                className={classes.submit}
                color="secondary">Вычесть 1</Button>
            <br />
            <br />
            <Divider />
            <br />

            {isLogged === false ?
                <>
                    <Typography variant="h3" className={classes.title}>
                        You are logged out
                    </Typography>

                    <Button
                        onClick={() => dispatch(login())}
                        variant="contained"
                        className={classes.submit}
                        color="primary"
                    >Войти</Button>
                </>
                : ''
            }

            {isLogged === true ?

                <>
                    <Typography variant="h3" className={classes.title}>
                        Now you are logged in
                    </Typography>

                    <Button onClick={() => dispatch(logout())}
                        variant="contained"
                        className={classes.submit}
                        color="primary"
                    >Выйти</Button>
                </>
                : ''
            }
        </>
    );
};
