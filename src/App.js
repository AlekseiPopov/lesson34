import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import NavigationBar from './components/NavigationBar';
import HomePage from './pages/HomePage';
import Login from './pages/Login';
import Register from './pages/Register';
import NotFound from './pages/NotFound';

function App() {
	return (
		<Router>
			<NavigationBar />
			<Switch>
				<Route path="/" exact component={HomePage} />
				<Route path="/login" component={Login} />
				<Route path="/register" component={Register} />
				<Route path="*" component={NotFound} />
			</Switch>
		</Router>
	);
}

export default App;
